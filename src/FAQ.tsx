import React, {FC} from 'react'

import slugify from 'react-slugify';
import Collapse from '@mui/material/Collapse'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'
import CollapseIcon from '@mui/icons-material/AddCircleOutlineRounded'

const FAQ = [
  {
    'title': 'What is the best place to grab a coffee?',
    'text': `Great question! ....`
  },
  {
    'title': 'How can I connect to a printer?',
    'text': <><b>HTML EXAMPLE</b>Great question! If you are on a Mac you will need the IP on the computer. If you are...</>
  }
]

const FAQSection: FC<{title:string, text: string|any}> = ({title, text}) => {
  const anchor = window.location.hash.replace('#', '')
  const slug = slugify(title)
  const [open, setOpen] = React.useState<boolean>(slug === anchor)
  
  console.log(anchor)
  return (
    <div style={{borderBottom: '1px solid #EEE', padding: 10}}>
      
      <div
        role='button'
        tabIndex={-1}
        onKeyPress={() => setOpen(!open)}
        onClick={() => setOpen(!open)}
        style={{cursor: 'pointer', display: 'flex', justifyContent: 'space-between', alignItems: 'baseline'}}
        >
        <a id={slug} href={'/#' + slug} style={{textDecoration: 'none', color: '#444'}}>
          <Typography 
            variant="h3"
            sx={{
              fontSize: '1.3em',
              '@media (max-width:600px)': {
                fontSize: '1em',
              },
            }}
            >
            {title}
            </Typography>
          </a>
        <CollapseIcon style={{opacity: .4}} />
      </div>
      <Collapse in={open}><p>{text}</p></Collapse>
    </div>
  )
}

const Page: FC = (props) => {
  
  return (
    <>
          <div style={{maxWidth: 800, margin: 'auto', padding: 20}}>
            {
              FAQ.map((props, i) => <FAQSection key={i} {...props} />)
            }
          </div>

          <div
            style={{cursor: 'pointer', maxWidth: 600, background: '#f9fafc', borderRadius: 20, padding: 20, margin: '50px auto'}}
            
            aria-label='get help'
            tabIndex={-1}
            role='button'
          >
            <div style={{padding: 20, margin: 'auto', textAlign: 'center'}}>
              <div style={{display: 'flex', justifyContent: 'center', position: 'relative'}}>
                <Avatar style={{width: 60, height: 60, right: -16, zIndex: 1, border: '3px solid rgb(249, 250, 252)'}} src='https://uploads-ssl.webflow.com/5f8a27fa913a100a39ea46f9/6427640a6c28f427c5d8f3f8_pretty_people_g1.png' />
                <Avatar style={{width: 70, height: 70, zIndex: 2, border: '3px solid rgb(249, 250, 252)'}} src='https://uploads-ssl.webflow.com/5f8a27fa913a100a39ea46f9/6427640a6a10ecae5f4c0b31_pretty_people_w1.png' />
                <Avatar style={{width: 60, height: 60, left: -16, zIndex: 1, border: '3px solid rgb(249, 250, 252)'}} src='https://uploads-ssl.webflow.com/5f8a27fa913a100a39ea46f9/6427640abde7a707bb7944d4_pretty_people_m2.png' />
              </div>
              <h3>Can't find the answer you're looking for? </h3>
              <p>
                Open a ticket and we will help answer your specific question.
              </p>
            </div>
          </div>

    </>
  )
}

export default Page
