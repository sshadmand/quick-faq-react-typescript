import React from 'react';
import './App.css';
import FAQ from './FAQ'
import AppBar from './AppBar'

function App() {
  return (
    <div>
      <AppBar />
      <FAQ />
    </div>
  );
}

export default App;
